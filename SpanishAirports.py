import sys
from pyspark import  SparkConf, SparkContext

if __name__ == "__main__":

    if len(sys.argv) != 2:
        print("Usage: spark-submit addnumbers.py <file>", file=sys.stderr)
        exit(-1)

    sparkConf = SparkConf()
    sparkContext = SparkContext(conf=sparkConf)
    sparkContext.setLogLevel("OFF")



    output=sparkContext\
        .textFile(sys.argv[1])\
        .map(lambda line: line.split(','))\
        .filter(lambda linea: linea[8]=="\"ES\"")\
        .map(lambda mapita:(mapita[2],1))\
        .reduceByKey(lambda a, b: a + b) \
        .sortByKey() \
        .collect()


    for (word, count) in output:
        print("%s: %i" % (word, count))

    sparkContext.stop()