import sys

from pyspark import SparkConf, SparkContext

if __name__ == "__main__":

    if len(sys.argv) != 2:
        print("Usage: spark-submit wordcount <file>", file=sys.stderr)
        exit(-1)

    sparkConf = SparkConf()
    sparkContext = SparkContext(conf=sparkConf)

    sparkContext.setLogLevel("OFF")

    output = sparkContext \
        .textFile(sys.argv[1]) \
        .map(lambda line: line.split(':'))\
        .map(lambda lin:(lin[0],(lin[2],lin[3]))) \
        .reduceByKey(lambda a, b: a + b) \
        .sortByKey().take(5)






    for (w, c) in output:
        print(w, c[0],c[1])

    sparkContext.stop()
